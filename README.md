# OpenID Foundation Documentation Tools

This is intended to be a baseline docker container that incorporates the documentation toolsets required for the management, maintenance and release of OpenID Foundation specifications and standards.

## "Features"

The following is included in this image:

- `xml2rfc`
- `mmark`
- A gulp script runner to facilitate automatic rebuilds of artefacts when changes are detected
- A web server with browser reload enabled so you can dev on your favourite IDE and see the changes live in a browser

## Running

While it is possible to simply execute from source the intent is to execute this container from a posted image so that focus can be given to making modifications to standards.

From a repository checkout (such as the FAPI repository):

```shell
docker run -it -p 3000:3000 --mount type=bind,source="$(pwd)",target=/opt/oidftools/source oidftools:latest
```

Once this is up and running you can visit http://localhost:3000/ for a directory list of the built artefacts.

## Getting Artefacts

Sometimes you might want to have the built artefacts land on your local directory. You can add an additional bind for the build directory to achieve this:

```shell
docker run -it -p 3000:3000 --mount type=bind,source="$(pwd)",target=/opt/oidftools/source --mount type=bind,source="$(pwd)/build",target=/opt/oidftools/build oidftools:latest
```
