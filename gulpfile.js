const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const path = require('path');
const tap = require('gulp-tap');
const fs = require('fs');

const src = {
    md: './source/**/*.md',
    html: './build/*.html'
};

// Static Server + watching md files
gulp.task('serve', serve);

// Run build process
gulp.task('build', build);

exports.default = serve;

if (!fs.existsSync("./build")) {
    fs.mkdirSync("./build");
}

function serve() {

    build(src.md);

    browserSync.init({
        server: "./build/",
        open: false,
        directory: true
    });

    gulp.watch([src.md]).on("change", function(path,stats) {
        console.log("Rebuilding " + path);
        build(path);
    });
    gulp.watch([src.html]).on('change', destTrigger);
};

function destTrigger() {
    reload({ stream: true });
}

function build(path) {
    return gulp
        .src(path, { read: false })
        .pipe(tap(markdown2rfc))
        .pipe(reload({ stream: true }));
}

function markdown2rfc(file) {
    const fileName = path.parse(file.basename).name;
    const execFile = require('child_process').execFile;
    console.log("Processing input file of " + file.path);
    const mmark = execFile('mmark', [file.path], (error, stdout, stderr) => {
        if (error) {
            console.log("mmark encountered an error processing " + file.path);
            console.error('stderr', stderr);
        } else {
            fs.writeFile(`build/${fileName}.xml`, stdout, function (err) {
                if (err) {
                    console.log("encountered error writing " + file.basename);
                    return console.log(err);
                }

                const xml2rfc = execFile('xml2rfc', ['-p', 'build/', '--html', `build/${fileName}.xml`], (error, stdout, stderr) => {
                    if (error) {
                        console.error('stderr', stderr);
                    } else {
                        console.log('XML2RFC Rebuild Completed', stdout);
                    }
                });
            });
        }
    });

    reload({ stream: true })

}


