FROM node:alpine

RUN apk add --no-cache wget tini libxml2-dev libxslt-dev gcc libxml2 libxslt musl-dev
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 python3-dev && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools xml2rfc
RUN npm install -g gulp

COPY . /opt/oidftools

WORKDIR /usr/local/bin

RUN wget https://github.com/mmarkdown/mmark/releases/download/v2.2.10/mmark_2.2.10_linux_amd64.tgz -O/tmp/mmark.tgz \
   && tar xzf /tmp/mmark.tgz

WORKDIR /opt/oidftools
RUN npm install
RUN mkdir -p /opt/oidftools/build
RUN mkdir -p /opt/oidftools/source
RUN chmod 755 /opt/oidftools/build

EXPOSE 3000/tcp

ENTRYPOINT ["/sbin/tini", "/usr/local/bin/gulp"]
